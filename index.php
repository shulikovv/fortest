<?php
function getData($fileName) {
    if (!$fileName) {
        return [];
    }
    $arr = [];
    $file = fopen($fileName, "r");
    if ($file) {
        while(!feof($file)) {
            $line = fgets($file);
            if (!empty($line) && strlen($line) > 4) {
                $arr[] = explode("|", $line);
            }
        }
        fclose($file);
    }
    return $arr;
};

function getChildren($data, $parentId) {
    $result = [];
    foreach ($data as $value) {
        if (is_array($value) && (int) $value[1] === (int) $parentId) {
            $result[] = $value;
        }
    }

    return $result;
}

function buildTree($arr, $level = 0, $parentId = 0) {
    $result = [];
    $items = getChildren($arr, $parentId);
    foreach ($items as $item) {
        $result = array_merge($result, [str_repeat(" ", $level).$item[2]], buildTree($arr, $level + 1, $item[0]));
    }
    return $result;
}

$data = getData("data.txt");
$result = buildTree($data);

foreach ($result as $item) {
    echo $item;
}
